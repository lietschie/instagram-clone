import 'package:flutter/material.dart';
import 'package:instagram_clone/insta_stories.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InstaList extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;

    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context,index) => index == 0 ? new SizedBox(
        child:new InstaStories(),
        height: deviceSize.height * 0.17,
      ): Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //1st row
          Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                          fit: BoxFit.fill,
                          image :new NetworkImage(
                            "https://instagram.fcgk15-1.fna.fbcdn.net/vp/bb13c78a7e58ec8358c3b8948a1dfc21/5BD3C1A4/t51.2885-19/s150x150/32901974_1099938690157505_5516148093071917056_n.jpg"
                          )
                        )
                      ),
                    ),
                    new SizedBox(
                      width: 10.0,
                    ),
                    new Text(
                      "ant_joshua"
                    )
                  ],
                ),
                new IconButton(
                  icon: Icon(Icons.more_vert),
                  onPressed: null,
                )
              ],
            ),
          ),
          //2ndrow
          Flexible(
            fit:FlexFit.loose,
            child: new Image.network(
              "https://instagram.fcgk15-1.fna.fbcdn.net/vp/d40c01786428398ddaaf51fb9d517ab6/5BC544FA/t51.2885-15/e35/34597668_2108369426118542_9007757428690780160_n.jpg",
              fit: BoxFit.cover,
            ),
          ),
          //3rd Row
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Icon(
                      FontAwesomeIcons.heart
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Icon(
                      FontAwesomeIcons.comment
                    ),
                    new SizedBox(
                      width: 16.0,
                    ),
                    new Icon(
                      FontAwesomeIcons.paperPlane
                    )
                  ],
                ),
                new Icon(FontAwesomeIcons.bookmark)
              ],
            ),
            
          ),
          //4th Row
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              "Liked by ant_joshua , and 1,000,000 others",
              style: TextStyle(
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          //5th Row
          Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0,8.0),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                      fit: BoxFit.fill,
                      image : new NetworkImage(
                        "https://instagram.fcgk15-1.fna.fbcdn.net/vp/bb13c78a7e58ec8358c3b8948a1dfc21/5BD3C1A4/t51.2885-19/s150x150/32901974_1099938690157505_5516148093071917056_n.jpg"
                      )
                    )
                    
                  ),
                ),
                new SizedBox(
                  width: 10.0,
                ),
                Expanded(
                  child: new TextField(
                    decoration: new InputDecoration(
                      border: InputBorder.none,
                      hintText: "Add a comment..."
                    ),
                  ),
                )
              ],
            )
          ),
          //6th Row
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child:
              Text("1 Day Ago",style: TextStyle(color: Colors.grey))
          )

        ],
      ),
    );
  }
}